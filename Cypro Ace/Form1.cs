﻿using DiffMatchPatch;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using static Enums;

namespace CyproAce
{
    public partial class Form1 : Form
    {
        public BindingList<string> MenuItems;

        private readonly string Body = "";
        private Dictionary<string, long> MaxLines;
        private Dictionary<long, FilterEntry> FilterEntries;
        private readonly Regex UnixLineBreak = new Regex(@"(?<!\r)(\n)");
        public ScriptIndexTable Scripts;
        private readonly DiffMatchPatch.DiffMatchPatch DIFF = new DiffMatchPatch.DiffMatchPatch();

        // these are the diffs
        private List<Diff> diffs;

        // chunks for formatting the two RTBs:
        private List<Chunk> chunkList1;
        private List<Chunk> chunkList2;

        // two color lists:
        private readonly Color[] colors1 = new Color[3] { Color.LightGreen, Color.LightSalmon, Color.White };
        private readonly Color[] colors2 = new Color[3] { Color.LightSalmon, Color.LightGreen, Color.White };

        private ScriptIndexTable.PointerTableEntry GetPointerTableEntry(FilterEntry f) => Scripts.IndexTable[IndexTableKey].PointerTable[f.PointerTableKey].PointerTableEntry[f.PointerTableEntryKey];
        private string IndexTableKey { get => (string)ComboBoxEntry.SelectedItem; }

        public string NewTextbox
        {
            get => ((System.Windows.Controls.TextBox)elementHost1.Child).Text;
            set => ((System.Windows.Controls.TextBox)elementHost1.Child).Text = value;
        }
        public int PaddingLength { get => MaxLines[(string)ComboBoxEntry.SelectedItem].ToString().Length; }

        public Form1()
        {
            InitializeComponent();

            System.Windows.Controls.TextBox richTextBox_new = new System.Windows.Controls.TextBox
            {
                Name = "richTextBox_new",
                VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Visible,
                FontFamily = new System.Windows.Media.FontFamily("Courier New"),
                FontSize = 14F,
                TextWrapping = System.Windows.TextWrapping.Wrap
            };

            richTextBox_new.SpellCheck.IsEnabled = true;
            elementHost1.Child = richTextBox_new;

            System.Windows.Controls.TextBox richTextBox_proof = new System.Windows.Controls.TextBox
            {
                Name = "richTextBox_proof",
                IsReadOnly = true,
                VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Visible,
                FontFamily = new System.Windows.Media.FontFamily("Courier New"),
                FontSize = 14F,
                TextWrapping = System.Windows.TextWrapping.Wrap
            };

            richTextBox_proof.SpellCheck.IsEnabled = true;
            ElementHost2.Child = richTextBox_proof;
        }

        private void ListBox_menu_SelectedIndexChanged(object sender = null, EventArgs e = null)
        {
#if (!DEBUG)
            try
            {
#endif
            int i = ListBoxMenu.SelectedIndex;

            if (i < 0)
            {
                return;
            }

            string text = ListBoxMenu.GetItemText(ListBoxMenu.SelectedItem);

            if (text == string.Empty)
            {
                throw new Exception("Listbox entry name cannot be empty");
            }

            string indexTableKey = (string)ComboBoxEntry.SelectedItem;

            int indexOfSeperator = text.IndexOf(Constants.SEPERATOR) + Constants.SEPERATOR.Length;

            TextBoxName.Text = text.Substring(indexOfSeperator, text.Length - indexOfSeperator);

            int CumulativeId = int.Parse(text.Substring(0, PaddingLength));

            FilterEntry filterEntry = FilterEntries[CumulativeId];
            var entry = Scripts.IndexTable[indexTableKey].PointerTable[filterEntry.PointerTableKey].PointerTableEntry[filterEntry.PointerTableEntryKey];

            if (entry.SamePointerAs != null)
            {
                SetEnabilityForRichTextboxes(false);
                TextBoxName.Enabled = button_edit_name.Enabled = button_save_to_RAM.Enabled = false;
            }
            else
            {
                SetEnabilityForRichTextboxes(true);
                TextBoxName.Enabled = button_edit_name.Enabled = button_save_to_RAM.Enabled = true;
            }

            if (Setting.Settings.Scripts.Original)
            {
                RichTextBoxOriginal.Text = entry.Text[ScriptTypes.Original][0];
            }

            if (Setting.Settings.Scripts.New)
            {
                SetTextFromNewTextBox(entry.Text[Enums.ScriptTypes.New][0]);
            }

            if (Setting.Settings.Scripts.Proof)
            {
                SetTextFromProofTextBox(entry.Text[Enums.ScriptTypes.Proof][0]);
            }

            if (Setting.Settings.Scripts.Comment)
            {
                string commentText = entry.Text[Enums.ScriptTypes.Comment][0];
                if (commentText != null && UnixLineBreak.IsMatch(commentText))
                {
                    commentText = commentText.Replace("\n", "\r\n");
                }

                RichTextBoxComment.Text = commentText;
            }

            UpdatePreview();

#if (!DEBUG)

        }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
#endif
        }

        private void UpdatePreview()
        {
            RichTextBoxPreviewNew.Text = CreatePreview(GetTextFromNewTextBox(), true);
            RichTextBoxPreviewProof.Text = CreatePreview(GetTextFromProofTextBox(), true);
            RichTextBoxPreviewOriginal.Text = CreatePreview(RichTextBoxOriginal.Text, false);


            if (Setting.Settings.Scripts.Proof)
            {
                HighlightProofreadAndNew();
            }
        }

        private void SetTextFromNewTextBox(string text)
        {
            var elementHost = this.elementHost1;
            System.Windows.Controls.TextBox WpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            WpfTextBox.Text = text;

            //Make sure the undo doesn't carry over from the previous entry.
            int originalValue = WpfTextBox.UndoLimit;
            WpfTextBox.UndoLimit = 0;
            WpfTextBox.UndoLimit = originalValue;
        }

        private void SetTextFromProofTextBox(string text)
        {
            var elementHost = this.ElementHost2;
            System.Windows.Controls.TextBox WpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            WpfTextBox.Text = text;

            //Make sure the undo doesn't carry over from the previous entry.
            int originalValue = WpfTextBox.UndoLimit;
            WpfTextBox.UndoLimit = 0;
            WpfTextBox.UndoLimit = originalValue;
        }

        private void SaveToDiskToolStripMenuItem_Click(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif
            File.WriteAllText(Setting.Settings.Scripts.ScriptPath, JsonConvert.SerializeObject(Scripts, Formatting.Indented));
            MessageBox.Show("Script saved.");
#if (!DEBUG)
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
#endif
        }

        private void LoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif
            using (OpenFileDialog fd = new OpenFileDialog() { Filter = "json|Settings.json" })
            {
                MaxLines = new Dictionary<string, long>();
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(fd.FileName))
                    {
                        string folder = Path.GetDirectoryName(fd.FileName);
                        Directory.SetCurrentDirectory(folder);

                        Setting.ReadSettingsFile(fd.FileName);
                        Scripts = JsonConvert.DeserializeObject<ScriptIndexTable>(new StreamReader(Setting.Settings.Scripts.ScriptPath).ReadToEnd());

                        if (!Setting.Settings.Scripts.Proof)
                        {
                            SplitContainer2.Panel2Collapsed = true;
                            SplitContainer2.Panel2.Hide();
                            CheckBoxProofread.Visible = false;
                        }
                        else
                        {
                            SplitContainer2.Panel2Collapsed = false;
                            SplitContainer2.Panel2.Show();
                            CheckBoxProofread.Visible = true;
                        }

                        while (ComboBoxEntry.Items.Count != 0)
                        {
                            ComboBoxEntry.Items.RemoveAt(0);
                        }

                        foreach (var kvp in Scripts.IndexTable)
                        {
                            ComboBoxEntry.Items.Add(kvp.Key);
                        }

                        // Assign cummulative id
                        foreach (var indexTableKvp in Scripts.IndexTable)
                        {
                            int cumulativeId = 0;
                            foreach (var PointerTableKvp in indexTableKvp.Value.PointerTable)
                            {
                                foreach (var PointerTableEntryKvp in PointerTableKvp.Value.PointerTableEntry)
                                {
                                    PointerTableEntryKvp.Value.CumulativeId = cumulativeId++;
                                }
                            }

                            MaxLines.Add(indexTableKvp.Key, cumulativeId - 1);
                        }

                        ComboBoxEntry.SelectedIndex = 0;

                        WriteAndLaunchGameToolStripMenuItem.Enabled = File.Exists(Setting.Settings.ScriptManager.WriteScript);
                        SplitContainer_main.Enabled = true;
                        SpreadsheetToolStripMenuItem.Enabled = true;
                        SaveToDiskToolStripMenuItem.Enabled = true;
                        OtherToolStripMenuItem.Enabled = true;
                    }
                }
            }

#if (!DEBUG)
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
#endif

        }

        private void SplitContainerOriginal_SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (Setting.Settings.Scripts.Proof)
            {
                SplitContainerProof.SplitterDistance = SplitContainerOriginal.SplitterDistance;
            }
            else
            {
                SplitContainerNew.SplitterDistance = SplitContainerOriginal.SplitterDistance;
            }
        }


        private void SplitContainer_proof_SplitterMoved(object sender, SplitterEventArgs e)
        {
            SplitContainerNew.SplitterDistance = SplitContainerProof.SplitterDistance;
        }

        private void SplitContainer_new_SplitterMoved(object sender, SplitterEventArgs e)
        {
            SplitContainerOriginal.SplitterDistance = SplitContainerNew.SplitterDistance;
        }

        private void ButtonEditNameClick(object sender, EventArgs e)
        {
            int i = ListBoxMenu.SelectedIndex;
            string text = ListBoxMenu.GetItemText(ListBoxMenu.SelectedItem);
            string menuName = TextBoxName.Text;
            int index = int.Parse(text.Substring(0, PaddingLength));

            MenuItems[i] = FormatMenuItem(index, menuName);

            FilterEntries[index].MenuText = menuName;
            GetPointerTableEntry(FilterEntries[index]).Text[Enums.ScriptTypes.Menu][0] = menuName;
        }

        private void FilterSearch(object sender = null, EventArgs e = null)
        {
            Enums.ScriptTypes scriptType;
            if (radioButton_menu.Checked)
            {
                scriptType = Enums.ScriptTypes.Menu;
            }
            else if (radioButton_Comment.Checked)
            {
                scriptType = Enums.ScriptTypes.Comment;
            }
            else if (radioButton_original.Checked)
            {
                scriptType = Enums.ScriptTypes.Original;
            }
            else if (radioButton_new.Checked)
            {
                scriptType = Enums.ScriptTypes.New;
            }
            else
            {
                throw new Exception("A script type radio button must be selected");
            }

            bool useRegex = checkBox_regex.Checked;
            bool isProofread = CheckBoxProofread.Checked;
            string searchText = textBox_search.Text;
            FilterEntries = new Dictionary<long, FilterEntry>();

            foreach (var pointerTableKvp in Scripts.IndexTable[IndexTableKey].PointerTable)
            {
                foreach (var pointerTableEntryKvp in Scripts.IndexTable[IndexTableKey].PointerTable[pointerTableKvp.Key].PointerTableEntry)
                {
                    if (!pointerTableEntryKvp.Value.Text.ContainsKey(scriptType) || pointerTableEntryKvp.Value.Text[scriptType].Count == 0)
                    {
                        throw new Exception($"PointerTableKey: {pointerTableKvp.Key}, PointerTableEntryKey: { pointerTableEntryKvp.Key }: No menu entry.");
                    }

                    List<string> scriptEntryText = pointerTableEntryKvp.Value.Text[scriptType];

                    bool pass = false;
                    if (useRegex)
                    {
                        Regex regex = new Regex(searchText);

                        foreach (string s in scriptEntryText)
                        {
                            if (regex.IsMatch(s))
                            {
                                pass = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach (string s in scriptEntryText)
                        {
                            string entryText = s;

                            if (entryText == null)
                            {
                                entryText = "";
                            }

                            if (entryText.Contains(searchText))
                            {
                                pass = true;
                                break;
                            }
                        }
                    }

                    if (!pass && scriptType == Enums.ScriptTypes.Menu)
                    {
                        pass = FormatMenuItemNumber(pointerTableEntryKvp.Value.CumulativeId).Contains(searchText);
                    }

                    if (pass && isProofread && pointerTableEntryKvp.Value.Text[Enums.ScriptTypes.Proof] != null && pointerTableEntryKvp.Value.Text[Enums.ScriptTypes.New] != null)
                    {
                        //Check if script entry arrays differs.
                        // Do this by convery the arrasy to strings and comparing those strings (JavaScript method)
                        pass = string.Join("|", pointerTableEntryKvp.Value.Text[Enums.ScriptTypes.Proof]) != string.Join("|", pointerTableEntryKvp.Value.Text[Enums.ScriptTypes.New]);
                    }

                    if (pass)
                    {
                        FilterEntries.Add(pointerTableEntryKvp.Value.CumulativeId, new FilterEntry(pointerTableKvp.Key, pointerTableEntryKvp.Key, pointerTableEntryKvp.Value.Text[Enums.ScriptTypes.Menu][0]));
                    }
                }
            }

            MenuItems = new BindingList<string>();

            foreach (var entry in FilterEntries)
            {
                MenuItems.Add(FormatMenuItem(entry.Key, entry.Value.MenuText));
            }

            ListBoxMenu.DataSource = MenuItems;

            if (ListBoxMenu.Items.Count == 0)
            {
                RichTextBoxOriginal.Text =
                RichTextBoxPreviewOriginal.Text =
                RichTextBoxPreviewProof.Text =
                RichTextBoxPreviewNew.Text =
                TextBoxName.Text =
                RichTextBoxComment.Text = "";

                SetTextFromNewTextBox("");
                SetTextFromProofTextBox("");
                SetEnabilityForRichTextboxes(false);

                TextBoxName.Enabled = button_edit_name.Enabled = button_save_to_RAM.Enabled = false;
            }
            else
            {
                TextBoxName.Enabled = button_edit_name.Enabled = button_save_to_RAM.Enabled = true;
                SetEnabilityForRichTextboxes(true);
            }
        }

        private string FormatMenuItem(long id, string menuText)
        {
            return string.Format("{0}{1}{2}", FormatMenuItemNumber(id), Constants.SEPERATOR, menuText);
        }

        private string FormatMenuItemNumber(long id)
        {
            return id.ToString("D" + PaddingLength);
        }

        private string GetTextFromNewTextBox()
        {
            //https://stackoverflow.com/questions/9650518/getting-textbox-data-from-a-control-hosted-in-winforms
            var elementHost = this.elementHost1;
            var wpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            return wpfTextBox.Text;
        }

        private string GetTextFromProofTextBox()
        {
            //https://stackoverflow.com/questions/9650518/getting-textbox-data-from-a-control-hosted-in-winforms
            var elementHost = this.ElementHost2;
            var wpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            return wpfTextBox.Text;
        }

        private void ImportExactToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportExcel(true);
        }

        private void importFuzzyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportExcel(false);
        }

        private void ImportExcel(bool isExact)
        {
#if (!DEBUG)
            try
            {
#endif
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "xlsx | *.xlsx"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                progressbar.Maximum = (int)MaxLines.ToList().Sum(itm => itm.Value) + MaxLines.Count;

                string filePath = openFileDialog.FileName;

                using (ExcelPackage excelPackage = new ExcelPackage(new FileInfo(filePath)))
                {
                    // Create a shallow copy. This way, in case of an error, everything can be rolled back.
                    var scripts_ = JsonConvert.DeserializeObject<ScriptIndexTable>(JsonConvert.SerializeObject(Scripts));

                    if (isExact && excelPackage.Workbook.Worksheets.Count != scripts_.IndexTable.Count)
                    {
                        throw new Exception($"Sheet mismatch: worksheet count(${ excelPackage.Workbook.Worksheets.Count }) !== script indexTable count(${  scripts_.IndexTable.Count})");
                    }

                    foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                    {
                        Dictionary<string, int> columnToId = new Dictionary<string, int>();

                        string indexTableKey = worksheet.Name;

                        for (int col = 1; ; col++)
                        {
                            string s = (string)worksheet.Cells[1, col].Value;

                            if (string.IsNullOrEmpty(s))
                            {
                                break;
                            }

                            columnToId.Add(s, col);
                        }

                        if (isExact)
                        {
                            // Check if all Enums.ColumnNames exist
                            foreach (var a in Constants.NecessaryColumnNames)
                            {
                                if (!columnToId.ContainsKey(a.ToString()))
                                {
                                    throw new Exception($"Missing column {a}. WorkSheet: {indexTableKey}");
                                }
                            }

                            // Check if all expected script types exist
                            foreach (ScriptTypes scriptType in Constants.ScriptTypes)
                            {
                                if (!IsScriptTypeInScriptDump(scriptType))
                                {
                                    continue;
                                }

                                if (!columnToId.ContainsKey(scriptType.ToString()))
                                {
                                    throw new Exception($"Missing column {scriptType}. WorkSheet: {indexTableKey}");
                                }

                                if (Constants.ScriptTypesWithPreview.Contains(scriptType))
                                {
                                    string previewColumnName = string.Format(Constants.PREVIEW, scriptType.ToString());

                                    if (!columnToId.ContainsKey(previewColumnName.ToString()))
                                    {
                                        throw new Exception($"Missing column {previewColumnName}. WorkSheet: {indexTableKey}");
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Check if the necesary Enums.ColumnNames exist
                            foreach (var a in Constants.NecessaryColumnNames)
                            {
                                if (!columnToId.ContainsKey(a.ToString()))
                                {
                                    throw new Exception($"Missing column {a}. WorkSheet: {indexTableKey}");
                                }
                            }
                        }

                        if (isExact)
                        {
                            int row = Constants.START_ROW;

                            foreach (var pointerTableKvp in scripts_.IndexTable[indexTableKey].PointerTable)
                            {
                                foreach (var pointerTableEntryKvp in pointerTableKvp.Value.PointerTableEntry)
                                {
                                    object readCell(string colId)
                                    {
                                        var retval = worksheet.Cells[row, columnToId[colId]].Value;

                                        if (retval == null)
                                        {
                                            return retval;
                                        }

                                        return retval.ToString();
                                    }

                                    var entry = pointerTableEntryKvp.Value;

                                    // More checks
                                    var cellValue = readCell(ColumnNames.CumulativeId.ToString());
                                    if (cellValue == null || !int.TryParse((string)cellValue, out int _) || entry.CumulativeId != int.Parse((string)cellValue))
                                    {
                                        throw new Exception($"CumulativeId mistmatch. Expected: {entry.CumulativeId}, Sheet: {cellValue}. Row: {row}, WorkSheet: {indexTableKey}");
                                    }

                                    cellValue = readCell(ColumnNames.Index.ToString());
                                    if (cellValue == null || !int.TryParse((string)cellValue, out int _) || pointerTableEntryKvp.Key != int.Parse((string)cellValue))
                                    {
                                        throw new Exception($"Index mistmatch. Expected: {pointerTableEntryKvp.Key}, Sheet: {cellValue}. Row: {row}, WorkSheet: {indexTableKey}");
                                    }

                                    cellValue = readCell(ColumnNames.PointerTableKey.ToString());

                                    if (cellValue == null || !MyMath.IsHex((string)cellValue) || pointerTableKvp.Key != MyMath.HexToDec((string)cellValue))
                                    {
                                        throw new Exception($"PointerTableKey mistmatch. Expected: {MyMath.DecToHex(pointerTableKvp.Key, Prefix.X)} of datatype string, Sheet: {cellValue} ({cellValue.GetType()}). Row: {row}, WorkSheet: {indexTableKey}");
                                    }

                                    foreach (var scriptType in Constants.AllScriptTypes)
                                    {
                                        if (!IsScriptTypeInScriptDump(scriptType))
                                        {
                                            continue;
                                        }

                                        cellValue = readCell(scriptType.ToString());

                                        if (cellValue != null && entry.Text[scriptType] == null)
                                        {
                                            throw new Exception($"ScriptType {scriptType} has a value of null but the sheet has a value. Row: {row}, WorkSheet: {indexTableKey}");
                                        }

                                        if (cellValue == null)
                                        {
                                            cellValue = "";
                                        }

                                        if (entry.Text[scriptType] != null)
                                        {
                                            entry.Text[scriptType][0] = cellValue.ToString();
                                        }
                                    }

                                    progressbar.Value++;
                                    row++;
                                }
                            }
                        }
                        else
                        {
                            int row = Constants.START_ROW;

                            while (true)
                            {
                                object readCell(string colId)
                                {
                                    var retval = worksheet.Cells[row, columnToId[colId]].Value;

                                    if (retval == null)
                                    {
                                        return retval;
                                    }

                                    return retval.ToString();
                                }

                                // If a blank field is encountered in the first column, consider the data to be done.
                                if (worksheet.Cells[row, 1].Value == null)
                                {
                                    break;
                                }

                                var pointerTableEntryKey_ = readCell(ColumnNames.Index.ToString());

                                if (pointerTableEntryKey_ == null || !int.TryParse((string)pointerTableEntryKey_, out int pointerTableEntryKey))
                                {
                                    throw new Exception($"Index pointerTableEntryKey. Sheet: {pointerTableEntryKey_}. Row: {row}, WorkSheet: {indexTableKey}");
                                }

                                var index = readCell(ColumnNames.PointerTableKey.ToString());
                                if (index == null || !MyMath.IsHex((string)index) || index.GetType() != typeof(string))
                                {
                                    throw new Exception($"pointerTable invalid datatype. Sheet: {index}. Row: {row}, WorkSheet: {indexTableKey}");
                                }

                                int pointerTableKey = MyMath.HexToDec((string)index);

                                foreach (var scriptType in Constants.AllScriptTypes)
                                {
                                    if (!IsScriptTypeInScriptDump(scriptType))
                                    {
                                        continue;
                                    }

                                    if (!columnToId.ContainsKey(scriptType.ToString()))
                                    {
                                        continue;
                                    }

                                    var entry = scripts_.IndexTable[indexTableKey].PointerTable[pointerTableKey].PointerTableEntry[pointerTableEntryKey];

                                    var cellValue = readCell(scriptType.ToString());
                                    if (cellValue != null && entry.Text[scriptType] == null)
                                    {
                                        throw new Exception($"ScriptType {scriptType} has a value of null but the sheet has a value. Row: {row}, WorkSheet: {indexTableKey}");
                                    }

                                    if (cellValue == null)
                                    {
                                        cellValue = "";
                                    }

                                    if (entry.Text[scriptType] != null)
                                    {
                                        entry.Text[scriptType][0] = cellValue.ToString();
                                    }
                                }

                                progressbar.Value++;
                                row++;
                            }
                        }
                    }
                    Scripts = scripts_;

                    // Update the menu and scripts in the textboxes by searching again.
                    FilterSearch();

                    MessageBox.Show("Excel import done.");
                    progressbar.Value = 0;
                }
            }

#if (!DEBUG)
        }
            catch (Exception e)
            {
                MessageBox.Show("Failed to import xlsx:" + e.Message);
                progressbar.Value = 0;
            }
#endif
        }

        private void ExportToolStripMenuItem_Click(object sender, EventArgs e)
        {
#if (!DEBUG)
            try
            {
#endif
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog
                {
                    Filter = "xlsx | *.xlsx",
                    FileName = "script.xlsx"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    progressbar.Maximum = (int)MaxLines.ToList().Sum(itm => itm.Value) + MaxLines.Count;
                    
                    Dictionary<string, int> columnToId = new Dictionary<string, int>();

                    int Col = 1;

                    foreach (ColumnNames s in Constants.ColumnNames)
                    {
                        columnToId.Add(s.ToString(), Col++);
                    }

                    foreach (ScriptTypes scriptType in Constants.ScriptTypes)
                    {
                        // If scriptType is specified to not have been dumped, don't validate if it's present in the excel file.
                        if (!IsScriptTypeInScriptDump(scriptType))
                        {
                            continue;
                        }

                        columnToId.Add(scriptType.ToString(), Col++);

                        if (Constants.ScriptTypesWithPreview.Contains(scriptType))
                        {
                            columnToId.Add(string.Format(Constants.PREVIEW, scriptType.ToString()), Col++);
                        }
                    }

                    foreach (var indexTableKvp in Scripts.IndexTable)
                    {
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(indexTableKvp.Key.ToString());
                        worksheet.Cells.Style.Font.Name = "Tahoma";
                        worksheet.Cells.Style.Font.Size = 8;

                        worksheet.Cells.Style.WrapText = true;

                        int row = 1;

                        // Write header
                        foreach (var kvp in columnToId)
                        {
                            worksheet.Cells[row, kvp.Value].Value = kvp.Key;

                            if (Enum.TryParse(kvp.Key, out Enums.ColumnNames _))
                            {
                                worksheet.Column(kvp.Value).Width = 10;
                            }
                            else
                            {
                                worksheet.Column(kvp.Value).Width = 50;
                            }
                        }

                        row++;

                        foreach (var pointerTableKvp in indexTableKvp.Value.PointerTable)
                        {
                            foreach (var pointerTableEntryKvp in pointerTableKvp.Value.PointerTableEntry)
                            {
                                // Make sure numerical values are written as strings.
                                // This offers interoperability with the JavaScript version
                                worksheet.Cells[row, columnToId[ColumnNames.CumulativeId.ToString()]].Value = pointerTableEntryKvp.Value.CumulativeId.ToString();
                                worksheet.Cells[row, columnToId[ColumnNames.Index.ToString()]].Value = pointerTableEntryKvp.Key.ToString();
                                worksheet.Cells[row, columnToId[ColumnNames.PointerTableKey.ToString()]].Value = MyMath.DecToHex(pointerTableKvp.Key, Prefix.X);
                                worksheet.Cells[row, columnToId[ColumnNames.Name.ToString()]].Value = pointerTableEntryKvp.Value.Text[ScriptTypes.Menu][0];

                                foreach (ScriptTypes scriptType in Constants.ScriptTypes)
                                {
                                    if (pointerTableEntryKvp.Value.Text.ContainsKey(scriptType))
                                    {
                                        worksheet.Cells[row, columnToId[scriptType.ToString()]].Value = pointerTableEntryKvp.Value.Text[scriptType][0];

                                        worksheet.Row(row).Height = Math.Max(worksheet.Row(row).Height, MeasureTextHeight(pointerTableEntryKvp.Value.Text[scriptType][0], worksheet.Cells.Style.Font, (int)worksheet.Column(columnToId[scriptType.ToString()]).Width));

                                        if (Constants.ScriptTypesWithPreview.Contains(scriptType))
                                        {
                                            string previewText = pointerTableEntryKvp.Value.Text[scriptType][0];
                                            switch (scriptType)
                                            {
                                                case ScriptTypes.Original:
                                                    previewText = CreatePreview(previewText, false);
                                                    break;
                                                case ScriptTypes.New:
                                                    previewText = CreatePreview(previewText, true);
                                                    break;
                                            }

                                            worksheet.Cells[row, columnToId[string.Format(Constants.PREVIEW, scriptType.ToString())]].Value = previewText;
                                        }
                                    }
                                }

                                progressbar.Value++;
                                row++;
                            }
                        }
                    }


                    excelPackage.SaveAs(new FileInfo(saveFileDialog.FileName));
                    MessageBox.Show("XLSX exported.");
                    progressbar.Value = 0;
                }
            }
#if (!DEBUG)
        }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
                progressbar.Value = 0;
            }
#endif
        }

        private bool IsScriptTypeInScriptDump(ScriptTypes scriptType)
        {
            return (Setting.Settings.Scripts.Original && scriptType == ScriptTypes.Original) ||
                        (Setting.Settings.Scripts.New && scriptType == ScriptTypes.New) ||
                        (Setting.Settings.Scripts.Comment && scriptType == ScriptTypes.Comment) ||
                        (Setting.Settings.Scripts.Proof && scriptType == ScriptTypes.Proof);
        }

        //https://stackoverflow.com/questions/41639278/autofit-row-height-of-merged-cell-in-epplus
        public double MeasureTextHeight(string text, ExcelFont font, int width)
        {
            if (string.IsNullOrEmpty(text)) return 0.0;
            var bitmap = new Bitmap(1, 1);
            var graphics = Graphics.FromImage(bitmap);

            var pixelWidth = Convert.ToInt32(width * 7.5);  //7.5 pixels per excel column width
            var drawingFont = new Font(font.Name, font.Size);
            var size = graphics.MeasureString(text, drawingFont, pixelWidth);

            //72 DPI and 96 points per inch.  Excel height in points with max of 409 per Excel requirements.
            return Math.Min(Convert.ToDouble(size.Height) * 72 / 96, 409);
        }

        private void SetEnabilityForRichTextboxes(bool isEnabled)
        {
            var elementHost = this.elementHost1;
            var WpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
            WpfTextBox.IsEnabled = isEnabled;

            elementHost = this.ElementHost2;
            WpfTextBox = (System.Windows.Controls.TextBox)elementHost.Child;
        }

        private void ButtonSaveToRAMClick(object sender, EventArgs e)
        {
            string text = (string)ListBoxMenu.SelectedItem;
            int index = int.Parse(text.Substring(0, PaddingLength));

            ScriptIndexTable.PointerTableEntry p = GetPointerTableEntry(FilterEntries[index]);
            p.Text[Enums.ScriptTypes.New][0] = GetTextFromNewTextBox();
            p.Text[Enums.ScriptTypes.Comment][0] = RichTextBoxComment.Text.Replace("\r\n", "\n");
            UpdatePreview();
        }

        private string CreatePreview(string input, bool IsNnew)
        {
            if (input == null)
            {
                return null;
            }

            string output = input;

            foreach (var Entry in Setting.Settings.ScriptManager.DisplayReplaceCollection)
            {
                string Find = Entry.Find;
                string Replace = "";

                if (Entry.Replace != null)
                {
                    Replace = Regex.Unescape(Entry.Replace);
                }
                else

                {
                    if (IsNnew)
                    {
                        if (Entry.ReplaceNew == null)
                        {
                            continue;
                        }

                        Replace = Regex.Unescape(Entry.ReplaceNew);
                    }
                    else
                    {
                        if (Entry.ReplaceOriginal == null)
                        {
                            continue;
                        }

                        Replace = Regex.Unescape(Entry.ReplaceOriginal);
                    }
                }

                output = Regex.Replace(output, Find, Replace);
            }

            List<string> multiplePatterns = new List<string> { @"\{(.*?)\}", @"\((.*?)\)" }; //remove special codes in parethesis.
            output = Regex.Replace(output, string.Join("|", multiplePatterns), string.Empty);
            return output;
        }

        private void FindAndReplaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormFindAndReplace f = new FormFindAndReplace(this);
            f.Show();
        }

        private void RunWriteScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string finalPath = $"\"{Path.GetFullPath(Setting.Settings.ScriptManager.WriteScript)}\"";

            if (!finalPath.StartsWith(@"//"))
            {
                try
                {
                    ThreadStart childref = new ThreadStart(() => ExecuteCommand(finalPath));
                    Thread childThread = new Thread(childref);
                    childThread.Start();
                }
                catch (ThreadAbortException)
                {
                    MessageBox.Show("Thread Abort Exception");
                }
            }
            else
            {
                MessageBox.Show($"Cannot execute script from fileshare: {Body}");
            }
        }

        private void ExecuteCommand(string finalPath)
        {
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + finalPath);
            var process = Process.Start(processInfo);
            process.WaitForExit();
            process.Close();
        }

        //https://stackoverflow.com/questions/24887238/how-to-compare-two-rich-text-box-contents-and-highlight-the-characters-that-are
        private void HighlightProofreadAndNew()
        {
            diffs = DIFF.Diff_main(RichTextBoxPreviewNew.Text, RichTextBoxPreviewProof.Text);
            DIFF.DiffCleanupSemanticLossless(diffs);

            chunkList1 = CollectChunks(RichTextBoxPreviewNew);
            chunkList2 = CollectChunks(RichTextBoxPreviewProof);

            PaintChunks(RichTextBoxPreviewNew, chunkList1);
            PaintChunks(RichTextBoxPreviewProof, chunkList2);

            RichTextBoxPreviewNew.SelectionLength = 0;
            RichTextBoxPreviewProof.SelectionLength = 0;
        }

        List<Chunk> CollectChunks(RichTextBox RTB)
        {
            RTB.Text = "";
            List<Chunk> chunkList = new List<Chunk>();
            foreach (Diff d in diffs)
            {
                if (RTB == RichTextBoxPreviewProof && d.operation == Operation.DELETE) continue;  // **
                if (RTB == RichTextBoxPreviewNew && d.operation == Operation.INSERT) continue;  // **

                Chunk ch = new Chunk();
                int length = RTB.TextLength;
                RTB.AppendText(d.text);
                ch.startpos = length;
                ch.length = d.text.Length;

                if (d.operation != Operation.EQUAL)
                {
                    ch.BackColor = RTB == RichTextBoxPreviewNew ? colors1[(int)d.operation]
                                               : colors2[(int)d.operation];
                }
                chunkList.Add(ch);
            }
            return chunkList;

        }

        void PaintChunks(RichTextBox RTB, List<Chunk> theChunks)
        {
            foreach (Chunk ch in theChunks)
            {
                RTB.Select(ch.startpos, ch.length);
                RTB.SelectionBackColor = ch.BackColor;
            }

        }

        private void ComboBoxEntry_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterSearch();
        }
    }
}