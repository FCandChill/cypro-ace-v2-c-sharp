﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CyproAce
{
    public partial class FormFindAndReplace : Form
    {
        private Form1 f;
        public FormFindAndReplace(Form1 f)
        {
            InitializeComponent();
            this.f = f;
        }

        private void textBoxFind_TextChanged(object sender, EventArgs e)
        {
            button_replace.Enabled = (textBox_find.Text != "");
        }

        private void button_replace_Click(object sender, EventArgs e)
        {
            f.NewTextbox = f.NewTextbox.Replace(textBox_find.Text, textBox_replace.Text);
        }
    }
}
