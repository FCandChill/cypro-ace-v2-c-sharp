﻿public class FilterEntry
{
    public long PointerTableKey { get; set; }
    public int PointerTableEntryKey { get; set; }
    public string MenuText { get; set; }

    public FilterEntry(long PointerTableKey, int PointerTableEntryKey, string MenuText)
    {
        this.PointerTableKey = PointerTableKey;
        this.PointerTableEntryKey = PointerTableEntryKey;
        this.MenuText = MenuText;
    }
}